# -*- coding: utf-8 -*-
"""argparse helper."""
import argparse
import os

__author__ = 'haochun.fu'
__date__ = '2020-01-05'


def files(value: str) -> str:
    """Check whether file(s) is exist or not.

    Args:
        value (str): Files are separated by ','.

    Returns:
        str: Value.

    Raises:
        argparse.ArgumentTypeError: If file is not exist.
    """
    for file in value.split(','):
        if not os.path.isfile(file):
            raise argparse.ArgumentTypeError(f"'{file}' is not found")
    return value
