# MIR lab job report

Submit job report.

## Setup

Install required Python modules.

```shell
pip install -r requirements.txt
```

---

## Job file

```json
{
  "completion": [
    "Completed job"
  ],
  "todo": [
    ["TODO", "Expected finish date in format Y/m/d"]
  ]
}
```

---

## User file

```
username
password
```

---

## User Agents file

Example

```
Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36
```

---

## Execution

```shell
python mirlab_job_report.py --data job.json
```

It will ask user name and password.

---

Provide user name and password with user file.

```shell
python mirlab_job_report.py --data job.json --user user
```

---

Use customized User Agents file.

```shell
python mirlab_job_report.py --data job.json --user_agents useragents.txt
```
