#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""Submit job report."""
import argparse
import copy
import datetime
import getpass
import json
import logging
import os
import random
import re
import time
from typing import (
    Dict,
    List,
    Union
)

from bs4 import BeautifulSoup
import requests

import argparse_helper


__author__ = 'haochun.fu'
__date__ = '2020-01-05'



class MirlabJobReportException(Exception):
    """Base exception of MirlabJobReport."""
    pass


class SignInError(MirlabJobReportException):
    """Failed to sign in."""
    pass


class SignOutError(MirlabJobReportException):
    """Failed to sign out."""
    pass


class SubmitError(MirlabJobReportException):
    """Failed to submit job report."""
    pass


class InvalidJobContent(MirlabJobReportException):
    """Invalid job content."""
    pass


class PageChanged(MirlabJobReportException):
    """Page is changed."""
    pass


class MirlabJobReport(object):
    """Submit job report."""

    """Base URL."""
    __BASE_URL = 'http://mirlab.org'

    """Max amount of records of each field."""
    __FIELD_MAX_AMOUNT = 5

    """Prefix name of field of finished."""
    __FIELD_FINISHED_PREFIX = 'finished'

    """Prefix name of field of this date."""
    __FIELD_THIS_DATE_PREFIX = 'thisDate'

    """Prefix name of field of this task."""
    __FIELD_THIS_TASK_PREFIX = 'thisTask'

    """Header of host."""
    __HEADER_HOST = 'mirlab.org'

    """Page of home."""
    __PAGE_HOME = 'index.asp'

    """Page of job view."""
    __PAGE_JOB_VIEW = 'work_login.asp'

    """Page of sign in."""
    __PAGE_SIGNIN = 'login.asp'

    """Page of sign out."""
    __PAGE_SIGNOUT = 'logout.asp'

    """Page of submission."""
    __PAGE_SUBMIT = 'work_register.asp'

    """Completion field in data file."""
    DATA_COMPLETION = 'completion'

    """Todo field in data file."""
    DATA_TODO = 'todo'

    """Encoding of site."""
    ENCODING = 'big5'


    def __init__(
        self,
        username: str,
        password: str,
        user_agent: str = None
    ) -> None:
        """Constructor.

        Args:
            username (str): Username.
            password (str): Password.
            user_agent (str): User-Agent. Default is None.
        """
        self.__user_agent = user_agent

        self.__req = requests.Session()

        self.signin(username, password)

    def __check_signed_in(self) -> bool:
        """Check whether it is signed in or not.

        Returns:
            bool: whether it is signed in or not.
        """
        return self.__req \
            .get(
                MirlabJobReport.__BASE_URL,
                headers=self.__get_headers()) \
            .text \
            .find(MirlabJobReport.__PAGE_SIGNOUT) >= 0

    def __encode_data(
        self,
        data: Union[dict, list, tuple]
    ) -> Union[dict, list, tuple]:
        """Encode data.

        Args:
            data (dict|list|tuple): Data.

        Returns:
            dict|list|tuple: Encoded data.
        """
        if isinstance(data, dict):
            ret = {}
            for key, val in data.items():
                ret[key] = val.encode(MirlabJobReport.ENCODING) \
                    if isinstance(val, str) else val
        elif isinstance(data, (list, tuple)):
            ret = []
            for row in data:
                ret.append(
                    (
                        row[0],
                        row[1].encode(MirlabJobReport.ENCODING) \
                            if isinstance(row[1], str) else row[1]
                    ))
        else:
            assert False, 'Invalid data type to be encoded'
        return ret

    def __get_headers(self, referer_page: str = None) -> Dict[str, str]:
        """Get headers.

        Args:
            referer_page (str): Referer page. Default is None.

        Returns:
            dict: Headers.
        """
        headers = {}
        if self.__user_agent:
            headers['User-Agent'] = self.__user_agent
        if referer_page is not None:
            headers['Referer'] = self.__get_page_url(referer_page)
        return headers

    def __get_page_url(self, page: str) -> str:
        """Get page URL.

        Args:
            page (str): Page.

        Returns:
            str: URL.
        """
        return os.path.join(MirlabJobReport.__BASE_URL, page)

    def chk_job_content(self, job: dict) -> bool:
        """Check whether job content is valid or not.

        Args:
            job (dict): Job.
                {
                    'MirlabJobReport.DATA_COMPLETION': ["job", ...],
                    'MirlabJobReport.DATA_TODO': [
                        ['job', 'date'],
                    ],
                }

        Returns:
            bool: Whether job content is valid or not.
        """
        for item in (
            MirlabJobReport.DATA_COMPLETION,
            MirlabJobReport.DATA_TODO):
            if item not in job \
               or len(job[item]) > MirlabJobReport.__FIELD_MAX_AMOUNT:
                return False
        for row in job[MirlabJobReport.DATA_TODO]:
            if len(row) != 2:
                return False

            if not row[0]:
                return False
            try:
                datetime.datetime.strptime(row[1], '%Y/%m/%d')
            except ValueError:
                return False
        return True

    def chk_submitted(self) -> bool:
        """Check whether job report is submitted or not.

        Returns:
            bool: Whether job report is submitted or not.

        Raises:
            PageChanged: If failed to parse target.
        """
        page = MirlabJobReport.__PAGE_JOB_VIEW
        res = self.__req.get(
            self.__get_page_url(page),
            headers=self.__get_headers())
        soup = BeautifulSoup(res.text, 'html.parser')
        form = soup.find('form', attrs={'name': 'upload'})
        if not form:
            raise PageChanged(f'{page} is changed')
        for item in ('finished', 'thisTask'):
            rows = form.find_all(
                'textarea',
                attrs={'name': re.compile(f'{item}\d+')})
            if not rows:
                raise PageChanged(f'{page} is changed')
            for row in rows:
                if row.text != '':
                    return True
        return False

    def signin(self, username: str, password: str) -> None:
        """Sign in.

        Args:
            username (str): Username.
            password (str): Password.

        Raises:
            SignInError: If failed to sign in.
        """
        res = self.__req.post(
            self.__get_page_url(MirlabJobReport.__PAGE_SIGNIN),
            headers=self.__get_headers(MirlabJobReport.__PAGE_HOME),
            data=self.__encode_data({
                'person': username,
                'password': password,
                'Submit': '登入',
            }))
        if res.status_code != 200:
            res.encoding = MirlabJobReport.ENCODING
            raise SubmitError(f'Status code: {res.status_code}\n{res.text}')
        if not self.__check_signed_in():
            raise SignInError('Failed to sign in')

    def signout(self) -> None:
        """Sign out.
        
        Raises:
            SignOutError: If failed to sign out.
        """
        self.__req.get(
            self.__get_page_url(MirlabJobReport.__PAGE_SIGNOUT),
            headers=self.__get_headers())
        if self.__check_signed_in():
            raise SignOutError('Failed to sign out')

    def submit(self, job: dict) -> None:
        """Submit job report.

        Args:
            job (dict): Job.
                {
                    'MirlabJobReport.DATA_COMPLETION': ["job", ...],
                    'MirlabJobReport.DATA_TODO': [
                        ['job', 'date'],
                    ],
                }

        Raises:
            InvalidJobContent: If job content is invalid.
            SubmitError: If failed to submit.
        """
        if not self.chk_job_content(job):
            raise InvalidJobContent('Invalid job content')

        if not self.__check_signed_in():
            raise SignInError('Not signed in')

        data = []
        completion = job[MirlabJobReport.DATA_COMPLETION]
        todo = job[MirlabJobReport.DATA_TODO]
        completion_len = len(completion)
        todo_len = len(todo)
        for no in range(MirlabJobReport.__FIELD_MAX_AMOUNT):
            no_s =  str(no)

            data.append(
                (
                    MirlabJobReport.__FIELD_FINISHED_PREFIX + no_s,
                    completion[no] if no < completion_len else ''
                ))

            data.append(
                (
                    MirlabJobReport.__FIELD_THIS_TASK_PREFIX + no_s,
                    todo[no][0] if no < todo_len else ''
                ))
            data.append(
                (
                    MirlabJobReport.__FIELD_THIS_DATE_PREFIX + no_s,
                    todo[no][1] if no < todo_len else ''
                ))

        res = self.__req.post(
            self.__get_page_url(MirlabJobReport.__PAGE_SUBMIT),
            headers=self.__get_headers(MirlabJobReport.__PAGE_JOB_VIEW),
            data=self.__encode_data(data))
        if res.status_code != 200:
            raise SubmitError(f'Status code: {res.status_code}\n{res.text}')

        if not self.chk_submitted():
            raise SubmitError('Failed to submit')


def __get_username_password(user_file: str = None) -> tuple:
    """Get username and password.

    If file is not given, read from input.

    Args:
        user_file (str): User file having username and password in separated
            lines. Default is None.

    Raises:
        ValueError: If data of user file is invalid.
    """
    if user_file:
        with open(user_file, 'r') as f:
            rows = f.read().strip().split('\n')
            if len(rows) != 2:
                raise ValueError('Invalid user file')
            username = rows[0]
            password = rows[1]
    else:
        username = input('username: ')
        password = getpass.getpass(prompt='password: ')

    return username, password

def __load_user_agents(file: Union[str, None]) -> List[str]:
    """Load User-Agents.

    Args:
        file (str|None): File. If None is given, use SCRIPT_DIR/useragents.txt.

    Returns:
        list: List of User-Agents. If default file is not found, return empty
            list.

    Raises:
        FileNotFoundError: If specified file is not found.
    """
    ret = []

    if file is None:
        file = os.path.join(
            os.path.dirname(os.path.realpath(__file__)),
            'useragents.txt')
    elif not os.path.isfile(file):
        raise FileNotFoundError(f"User-Agent file '{file}' is not found")

    if not os.path.isfile(file):
        return ret

    with open(file, 'r') as f:
        for row in f:
            row = row.strip()
            if row:
                ret.append(row)
    return ret


def main(args: argparse.Namespace) -> None:
    """Execution.

    Args:
        args (argparse.Namespace): Arguments.
    """
    logging.basicConfig(
        level=logging.INFO,
        format='%(asctime)s [%(levelname)s] %(message)s',
        datefmt='%Y-%m-%d %H:%M:%S')
    if args.silent:
        logging.disable(logging.CRITICAL)

    start_time = time.time()

    user_agents = __load_user_agents(args.user_agents)
    user_agent = random.choice(user_agents) if user_agents else None

    username, password = __get_username_password(args.user)

    logging.info('Load job ...')
    with open(args.data, 'r') as f:
        job = json.load(f)

    reporter = MirlabJobReport(username, password, user_agent)
    logging.info('Submit ...')
    reporter.submit(job)

    elapsed_time = time.time() - start_time

    logging.info('Complete')
    logging.info(f'Elapsed time: {elapsed_time}s')



if __name__ == '__main__':
    arg_parser = argparse.ArgumentParser(description='Submit job report.')
    arg_parser.add_argument(
        '--data',
        type=argparse_helper.files,
        required=True,
        help='Job.')
    arg_parser.add_argument(
        '--silent',
        action='store_true',
        default=False,
        help='Quiet output.')
    arg_parser.add_argument(
        '--user',
        type=argparse_helper.files,
        help='File having username and password in separated lines.')
    arg_parser.add_argument(
        '--user_agents',
        help='File having rows of user agents. Default is'
             ' SCRIPT_DIR/useragents.txt')

    try:
        main(arg_parser.parse_args())
    except (MirlabJobReportException, FileNotFoundError) as e:
        print(e)
